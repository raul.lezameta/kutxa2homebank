# kutxa2homebank

This repository contains a script to convert a xls exported from Kutxabank to a csv file that can be imported in Homebank.


The Windows version of the script is compiled with pyinstaller and can be downloaded [here](/dist/kutxa2homebank.exe).


> To compile the script yourself, you need to install pyinstaller and run the following command:
> ```bash
> pyinstaller --onefile kutxa2homebank.py
> ```
