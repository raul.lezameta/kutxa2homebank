import pandas as pd
import tkinter as tk
from tkinter import filedialog
from pathlib import Path

if __name__ == "__main__":
    root = tk.Tk()
    root.withdraw()  # Hide the main window
    filename = Path(
        filedialog.askopenfilename(
            title="Select file",
            filetypes=(("Excel files", "*.xlsx *.xls"), ("All files", "*.*")),
        )
    )

    data = pd.read_excel(
        filename,
        header=6,
        usecols="A:E",
    )[1:]

    data_homebank = pd.DataFrame(
        data={
            "Date": pd.to_datetime(data["fecha"], format="%d/%m/%Y"),
            "Payment": [4 for i in range(len(data))],
            "Info": data["concepto"],
            "Payee": ["Add payee - imported kutxa"] * len(data),
            "Memo": [""] * len(data),
            "Amount": data["importe"],
            "Category": ["Add category - imported kutxa"] * len(data),
            "Tags": ["importedKutxa"] * len(data),
        }
    )

    data_homebank["Date"] = data_homebank["Date"].dt.strftime("%y-%m-%d")

    out_filename = filename.parent / (filename.stem + "_2homebank.csv")

    data_homebank.to_csv(out_filename, index=False, header=True, sep=";")
